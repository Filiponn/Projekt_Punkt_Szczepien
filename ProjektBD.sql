create database szczepienia;
use szczepienia;
select * from pacjenci;
select * from zlecenia_szczepienia;
select * from mozliwosc_szczepienia;
select * from archiwum_szczepienia;
select * from terminy;
select * from szczepionki;
drop table mozliwosc_szczepienia;
drop table zlecenia_szczepienia;
drop table pacjenci;
drop table archiwum_szczepienia;
drop table terminy;
drop table szczepionki;


create table pacjenci(
pesel varchar(11) primary key,
imie varchar(45),
nazwisko varchar(45),
numer_telefonu varchar(9),
data_urodzenia DATE
);

create table szczepionki(
id_szczepionki int primary key,
typ_szczepionki varchar(50)
);

create table terminy(
termin DATETIME
);
select typ_szczepionki, count(typ_szczepionki) from archiwum_szczepienia where termin between "2022-01-01" and "2022-06-06" group by typ_szczepionki;

create table zlecenia_szczepienia(
numer_zlecenia int auto_increment primary key,
pesel varchar(11),
foreign key (pesel) references pacjenci(pesel),
termin DATETIME,
typ_szczepionki varchar(50)
);

create table archiwum_szczepienia(
numer_zlecenia int,
pesel varchar(11),
termin DATETIME,
typ_szczepionki varchar(50),
realizacja enum('zrealizowane', 'anulowane') -- jeśli usunieteo co najmniej 16 minut przed szczepieniem to anulowane
);

create table mozliwosc_szczepienia(
pesel varchar(11),
czy_moze_szczepic enum('true','false'),
typ_szczepionki varchar(50)
);
INSERT INTO pacjenci(pesel, imie, nazwisko, numer_telefonu, data_urodzenia)
VALUES
('133232342', 'Adam', 'Kowalski', '500346333', '2000-10-20');
insert into szczepionki(id_szczepionki, typ_szczepionki)
values(12,'Krztusiec');
insert into terminy(termin)
values('2022-08-12');
SET sql_safe_updates = 0;
INSERT INTO terminy(termin)
VALUES
('2022.08.12 12:00');
INSERT INTO zlecenia_szczepienia(pesel, termin, typ_szczepionki)
VALUES
('133232342', '2022-10-12', 'Pfizer');
Delete from terminy where termin ='2022.08.12 12:00';
Delete from zlecenia_szczepienia where numer_zlecenia = 17; 


INSERT INTO pacjenci(pesel, imie, nazwisko, numer_telefonu, data_urodzenia)
VALUES
('166232349', 'Marek', 'Czochral', '500434444', '2000-12-27');
SET sql_safe_updates = 0;
INSERT INTO zlecenia_szczepienia(pesel, termin, typ_szczepionki, id_szczepionki)
VALUES
('166232349', '2018-02-13', 'Pfizer','2313');
Delete from zlecenia_szczepienia where pesel = '166232349'; 

INSERT INTO terminy(id_szczepionki,termin, typ_szczepionki)
VALUES
('2313','2022-02-13', 'Pfizer');


DELIMITER $$
CREATE TRIGGER before_insert_zlecenie_szczepienia
before insert
ON archiwum_szczepienia FOR EACH ROW
Begin
declare wiek int;
declare ostatnie_szczepienie datetime;
declare ostatnia_szczepionka varchar(50);
declare roznica_dni int;
declare realizacja1 varchar(12);

set wiek = (SELECT TIMESTAMPDIFF(YEAR, data_urodzenia, NOW()) FROM pacjenci where pesel = new.pesel);
set ostatnia_szczepionka = (SELECT typ_szczepionki FROM archiwum_szczepienia where pesel = new.pesel order by termin DESC limit 1);
set roznica_dni = (SELECT datediff(curdate(), termin) FROM archiwum_szczepienia where pesel = new.pesel order by termin DESC limit 1);
set realizacja1 = (SELECT realizacja FROM archiwum_szczepienia where pesel = new.pesel order by termin DESC limit 1);

     IF wiek >= 18 and ostatnia_szczepionka != new.typ_szczepionki and roznica_dni > 20 or 
     wiek >= 18 and ostatnia_szczepionka = new.typ_szczepionki and roznica_dni > 364 or realizacja1 = 'anulowane' THEN
        update mozliwosc_szczepienia
        set czy_moze_szczepic = 'true' where pesel = new.pesel;
	Else 
    update mozliwosc_szczepienia
        set czy_moze_szczepic = 'false' where pesel = new.pesel;
    END IF;
    
END$$
DELIMITER ;

drop trigger before_insert_zlecenie_szczepienia;

drop trigger after_insert_pacjenci;

DELIMITER $$
CREATE TRIGGER afterdel
after delete
ON zlecenia_szczepienia FOR EACH ROW
BEGIN
insert into terminy(termin) values (old.termin);
END$$
DELIMITER ;
drop trigger afterdel;



DELIMITER $$
CREATE TRIGGER afteru
after update
ON zlecenia_szczepienia FOR EACH ROW
BEGIN
insert into terminy(termin) values (old.termin);
END$$
DELIMITER ;

drop trigger afteru;

DELIMITER $$
CREATE TRIGGER after_insert_pacjenci
after insert
ON pacjenci FOR EACH ROW
Begin
declare wiek int;
set wiek = (SELECT TIMESTAMPDIFF(YEAR, data_urodzenia, NOW()) FROM pacjenci where pesel = new.pesel);

IF wiek >= 18 then
	INSERT INTO mozliwosc_szczepienia(pesel, czy_moze_szczepic, typ_szczepionki)
    VALUES(new.pesel, 'true', null);
else
INSERT INTO mozliwosc_szczepienia(pesel, czy_moze_szczepic, typ_szczepionki)
    VALUES(new.pesel, 'false', null);
    end if;
END$$
DELIMITER ;
drop trigger after_insert_pacjenci;

DELIMITER $$
CREATE TRIGGER after_insert_archiwum_szczepienia
after insert
ON archiwum_szczepienia FOR EACH ROW
BEGIN
declare wiek int;
declare ostatnie_szczepienie datetime;
declare ost_typ varchar(50);
declare roznica_dni int;
declare realizacja1 varchar(12);

set wiek = (SELECT TIMESTAMPDIFF(YEAR, data_urodzenia, NOW()) FROM pacjenci where pesel = new.pesel);
set roznica_dni = (SELECT datediff(curdate(), termin) FROM archiwum_szczepienia where pesel = new.pesel order by termin DESC limit 1);
set ost_typ = (SELECT typ_szczepionki FROM archiwum_szczepienia where pesel = new.pesel order by termin DESC limit 1);
set realizacja1 = (SELECT realizacja FROM archiwum_szczepienia where pesel = new.pesel order by termin DESC limit 1);

    IF wiek >= 18 and ost_typ != new.typ_szczepionki and roznica_dni > 20 or 
    wiek >= 18 and ost_typ = new.typ_szczepionki and roznica_dni > 364 or realizacja1 = 'anulowane' THEN
        update mozliwosc_szczepienia
        set czy_moze_szczepic = 'true' where pesel = new.pesel;
        update mozliwosc_szczepienia
        set typ_szczepionki = ost_typ where pesel = new.pesel;
	Else 
    update mozliwosc_szczepienia
        set czy_moze_szczepic = 'false' where pesel = new.pesel;
        update mozliwosc_szczepienia
        set typ_szczepionki = ost_typ where pesel = new.pesel;
    END IF;
END$$
DELIMITER ;
drop trigger after_insert_archiwum_szczepienia;
DELIMITER $$
CREATE TRIGGER after_zlecenie_szczepienia_delete
before delete
ON zlecenia_szczepienia FOR EACH ROW
Begin
declare minuty int;
set minuty = (SELECT TIMESTAMPDIFF(minute, now(), old.termin) FROM zlecenia_szczepienia where numer_zlecenia = old.numer_zlecenia);

if minuty > 15 then
	INSERT INTO archiwum_szczepienia(pesel, termin, typ_szczepionki, realizacja, numer_zlecenia)
    VALUES(old.pesel, old.termin, old.typ_szczepionki, 'anulowane',old.numer_zlecenia);
    else
    INSERT INTO archiwum_szczepienia(pesel, termin, typ_szczepionki, realizacja,numer_zlecenia)
    VALUES(old.pesel, old.termin, old.typ_szczepionki, 'zrealizowane',old.numer_zlecenia);
    end if;
END$$
DELIMITER ;

drop trigger after_zlecenie_szczepienia_delete;

create view pacjenci_terminy as 
select p.pesel as pesel, p.imie as imie, p.nazwisko as nazwisko, s.typ as typ_szczepionki, s.termin as termin from pacjenci p
join szczepionki s on p.pesel = s.typ;
drop view ap_pacjenci;
select * from ap_pacjenci;

create view archiwum as 
select a.pesel as pesel, s.typ_szczepionki as typ, a.realizacja as realizacja, a.termin as termin from archiwum_szczepienia a join szczepionki s on a.id_szczepionki = s.id_szczepionki;
select * from archiwum where pes5rel = '133232342';
drop view archiwum;

create view wolneTerminy as 
select typ_szczepionki as typ_szczepionki, termin as termin from terminy;
select * from wolneTerminy;
drop view wolneTerminy;
create view twojeTerminy as
select pesel as pesel, typ_szczepionki as typ_szczepionki, termin as termin
from zlecenia_szczepienia;
select * from twojeTerminy;
drop view twojeTerminy;

select typ_szczepionki, termin from twojeTerminy where pesel like '133232342';

CREATE USER 'ProjektBDAdmin'@'localhost' identified by 'Admin123';
Grant ALL PRIVILEGES ON szczepienia.* to 'ProjektBDAdmin'@'localhost';
SHOW GRANTS FOR 'ProjektBDAdmin'@'localhost';
drop user 'ProjektBDAdmin'@'localhost';

CREATE USER 'pracownik'@'localhost' IDENTIFIED BY 'Praca123';
GRANT ALL PRIVILEGES ON szczepienia.terminy TO 'pracownik'@'localhost';
GRANT ALL PRIVILEGES ON szczepienia.archiwum_szczepienia TO 'pracownik'@'localhost';
GRANT ALL PRIVILEGES ON szczepienia.zlecenia_szczepienia TO 'pracownik'@'localhost';

CREATE USER 'michal98'@'localhost' IDENTIFIED BY 'haslo21';
GRANT INSERT ON szczepienia.pacjenci TO 'michal98'@'localhost';
GRANT SELECT ON szczepienia.archiwum_szczepienia TO 'michal98'@'localhost';
GRANT SELECT ON szczepienia.terminy TO 'michal98'@'localhost';
GRANT SELECT ON szczepienia.mozliwosc_szczepienia TO 'michal98'@'localhost';
GRANT SELECT ON szczepienia.szczepionki TO 'michal98'@'localhost';
GRANT SELECT ON szczepienia.zlecenia_szczepienia TO 'michal98'@'localhost';
GRANT SELECT ON szczepienia.twojeTerminy TO 'michal98'@'localhost';
GRANT INSERT ON szczepienia.zlecenia_szczepienia TO 'michal98'@'localhost';
GRANT DELETE ON szczepienia.zlecenia_szczepienia TO 'michal98'@'localhost';
GRANT DELETE ON szczepienia.terminy TO 'michal98'@'localhost';
GRANT UPDATE ON szczepienia.zlecenia_szczepienia TO 'michal98'@'localhost';

drop user 'michal98'@'localhost';

CREATE USER 'andrej2008'@'localhost' IDENTIFIED BY 'haselko';
GRANT INSERT ON szczepienia.pacjenci TO 'andrej2008'@'localhost';
GRANT SELECT ON szczepienia.archiwum_szczepienia TO 'andrej2008'@'localhost';
GRANT SELECT ON szczepienia.terminy TO 'andrej2008'@'localhost';
GRANT SELECT ON szczepienia.mozliwosc_szczepienia TO 'andrej2008'@'localhost';
GRANT SELECT ON szczepienia.szczepionki TO 'andrej2008'@'localhost';
GRANT SELECT ON szczepienia.zlecenia_szczepienia TO 'andrej2008'@'localhost';
GRANT SELECT ON szczepienia.twojeTerminy TO 'andrej2008'@'localhost';
GRANT INSERT ON szczepienia.zlecenia_szczepienia TO 'andrej2008'@'localhost';
GRANT DELETE ON szczepienia.zlecenia_szczepienia TO 'andrej2008'@'localhost';
GRANT DELETE ON szczepienia.terminy TO 'andrej2008'@'localhost';
GRANT UPDATE ON szczepienia.zlecenia_szczepienia TO 'andrej2008'@'localhost';
drop user 'andrej2008'@'localhost';
